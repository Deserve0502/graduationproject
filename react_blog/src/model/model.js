import React from 'react';
import { observable, action, toJS } from 'mobx';
import { MessageBox } from 'element-react';
import store from '../store';
import { message } from 'antd';

class Model {
    //控制发现页查看更多是否开启
    @observable  FoundVisible = false;
    
    //点击查看更多获取当前点击列表
    @observable FoundDrawList = [];

    //加载更多数量(包含起始数量)
    @observable LoadCount = 12;

    //加载更多每次加载增加数量
    @observable count = 12;

    //打开发现页查看更多 的loading
    // @observable initLoading = true;

    //加载更多列表
    @observable FoundLoadList = [];

 

    //展开发现页 查看更多
    showDrawer = (list) => {
       this.FoundVisible = true;
       this.FoundDrawList = list;
       this.FoundLoadList = this.FoundDrawList.article.slice(0,this.LoadCount);
      
    //    this.initLoading = false;
      };
    
    //发现页 加载更多
      onLoadMore = () => {
        this.LoadCount += this.count;
        if(this.LoadCount > this.FoundDrawList.article.length){
            message.warning('没有更多啦～');
        }else{
            this.FoundLoadList = this.FoundDrawList.article.slice(0,this.LoadCount);
        }
      
        
        
        
      }
}
export default Model;
