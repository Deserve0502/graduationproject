import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
import store from '../../store'
import { TagOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react'
@observer
export default class UserLabel extends Component {
    componentDidMount(){
        store.getUserLabel();
     }
    render() {
        return (
            <div className='user-label-contain'>
            <p className='user-label-header'><TagOutlined />
            <span style={{marginLeft:'15px'}}>我的标签</span></p>
            <div className='user-label-list-contain'>
            {/* userLabel */}
            {
            store.userLabel.map((ele, index) => {
                      return (
            <div className='user-label-list' key={index}>
                {ele}
            </div>
           
                      )})}
            </div>
            </div>
)
}
}