import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
import store from '../../store'
import { observer } from 'mobx-react'
import { PlusOutlined, RedoOutlined } from '@ant-design/icons';
@observer
export default class HotUser extends Component {
    componentDidMount(){
        store.getHotUsers();
    }
    render() {
        return (
            <div className='suggested-users hot-user'>

            <div className='suggested-users-header'>
            <span>热门用户</span>
            <span>•</span>
            <span>综合排序</span>
            </div>

            <ul>
                <li className='suggested-users-list-li'>
                {
            store.hotUsers.map((ele, index) => {
                      return (
                        <span className='suggested-users-list'
                        key={index}>
                    <span className='suggested-users-list-img'>
                    <img src={ele.img} alt=""/>
                    </span>
                    <span className='suggested-users-list-content'>
                        <span>{ele.name}</span>
                        <span>{ele.intro}</span>
                        <span>{ele.status}</span>
                    </span>
                    <div className='suggested-users-list-attention'>
                    <PlusOutlined /> 关注
                </div>
                </span>
           
                      )})}
              
               
                
                </li>
               
               

            </ul>
            <div style={{display:'flex',justifyContent:'center',alignItems:'center',marginTop:'-15px'}} >
            <div className='trending-lookmore' style={{zIndex:'0',fontSize:'14px',margin:'auto'}} >换一批
            <span style={{marginBottom:'-3px'}}><RedoOutlined /></span></div>
            </div>
            </div>
)
}
}

