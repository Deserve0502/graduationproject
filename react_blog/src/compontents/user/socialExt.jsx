import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
//import store from 
import store from '../../store'
import { observer } from 'mobx-react';
import { EyeOutlined, MessageOutlined, FireOutlined } from '@ant-design/icons';
@observer
export default class  SocialExt extends Component {

    componentDidMount(){
        store.getSocialList();
     }

    render() {
        return (
            <div className='social-ext-contain'>
            <div className='social-ext-title'>
                好友动态
            </div>
            {
store.socialList.map((ele, index) => {
                      return (
            <div className='social-list' key={index}>
            <div className='social-list-content'>
            <span>{ele.title}</span>
            <span>{ele.name}</span>
            <span>{ele.time}</span>
            <span style={{display:'flex',justifyContent:'space-around',alignItems:'center'}}>

            <EyeOutlined/>{ele.look}
            <MessageOutlined />{ele.reply}
            </span>
            </div>
            <div className='social-list-img'>
            <img src="../userImg/IMG_6385.jpg" alt=""/>
            </div>
            </div>
                      )})}
                     <span className='social-more'> 查看更多</span>
            </div>
)
}
}