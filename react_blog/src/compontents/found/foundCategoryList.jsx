import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
import store from '../../store'
import '../../style/found.css'
import { EyeOutlined, MessageOutlined, LikeOutlined, FireOutlined, FileImageOutlined } from '@ant-design/icons';
import '../../style/tips/iconfont.css'
import { observer } from 'mobx-react';
import { Popover, Button,Pagination } from 'antd';
import http from '../../http/index'

@observer
export default class FoundCategoryList extends Component {
  
    state = {
        authorName:store.authorMess.authorName,
        authorImg:'',
        authorCity:'',
        authorPross:'',
        heat:'',
        imgNumber:'',
        fans:'',
    }
    componentDidMount(){
       store.getFoundArticle();
    }
   
    render() {
        const text = 
        <div className='cate-author-tips-name'>
            <div className='cate-auhotor-img'>
                <img src="../foundImg/sport.jpeg" />
            </div>
            <div className='cate-author-intro'>
                <span className='cate-author-intro-name'>
                {store.authorMess.authorName}
                </span>
                <span>
                    <span className='cate-author-intro-city'>
                        {store.authorMess.authorCity}
                    </span>
                    <span >
                        ｜
                     </span>
                    <span className='cate-author-intro-pross'>
                    {store.authorMess.authorPross}
                    </span>                      
                </span>
            </div>
            <div className='cate-author-tips-follow'>
                关注 +
            </div>
        </div>;
         const content = (
            <div className='cate-author-tips-content'>
                <p>其他文章</p>
                {

                }
                <p>这是一条文章标题这是一条文章标题这是一条文章标题</p>
                <p>这是二条文章标题</p>
                <p>这是三条文章标题</p>
                <div className='cate-author-tips-icon iconfont'>
                    <span>
                        <FireOutlined /> {store.authorMess.heat}
                    </span>
                    <span>
                        &#xe6eb; {store.authorMess.imgNumber}
                    </span>
                    <span>
                        &#xe6cf; {store.authorMess.fans}
                     </span>
                </div>
            </div>
        );
        return (
            <div className='safe'>
                <div className='cate-contain'>

                  
                    { 
                    
                    store.foundArticle.map((ele, index) => {
                      return (
                        <li className='cate-list' key={index}>
                        <div className='cate-list-img'>
                            <img src={ele.articlImg} />
                        </div>
                        <div className='cate-list-main'>
                            <div className='cate-list-title'>
                               {ele.articleTitle}
            </div>
                            <div className='cate-list-classify'>
                            {ele.tab}
            </div>
                            <div className='cate-list-icon'>
                                <span>
                                    <EyeOutlined />
                {ele.view}
                </span>
                                <span>
                                    <MessageOutlined />
                {ele.comments}
                </span>
                                <span>
                                    <LikeOutlined />
                {ele.thumb}
                </span>
                            </div>
                        </div>
                        <div className='cate-list-auhotor'>
                            <Popover placement="topLeft" title={text} content={content}
                          
                                overlayClassName='cate-author-tips'
                                getPopupContainer={
                                    () => document.querySelector('.cate-list-auhotor')
                                }
                            >
                                <div className='cate-auhotor-img'
                                onMouseEnter={()=>store.getFoundTips(ele.articleId)}
                                >

                                    <img src="../foundImg/sport.jpeg" />

                                </div>
                            </Popover>
                            <div className='cate-auhotor-name'>
                               {ele.authorName}
                            </div>
                            <div className='cate-auhotor-time'>
                                60分钟前
                            </div>
                        </div>
                    </li>      
                                                )

                      

                    }
                    )
                
                  }
                  <Pagination defaultCurrent={8} 
                  total={50} 
                        className='popular-pagin'
                        />        
                </div>
            </div>
        )
    }
}