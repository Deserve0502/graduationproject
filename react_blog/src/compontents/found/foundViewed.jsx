import React, { Component } from 'react'
//import ReactDom from 'react-dom'
import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
import store from '../../store'
import InfiniteScroll from 'react-infinite-scroller';
import { observer } from 'mobx-react'
import { List, Avatar, Space } from 'antd';
import { MessageOutlined, EyeOutlined, LikeOutlined  } from '@ant-design/icons';


const IconText = ({ icon, text }) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  );

@observer
export default class FoundViewed extends Component {
  componentDidMount(){
    store.getFoundViewed();
    
 }
    render() {
        return (
            <div className='found-viewed-contain'>
            <div style={{backgroundColor:'white',borderRadius:'10px',padding:'30px',marginBottom:'30px'}}>
            <List
    itemLayout="vertical"
    size="large"
    pagination={{
      onChange: page => {
        console.log(page);
      },
      pageSize: 3,
    }}
    dataSource={store.foundViewed}
    renderItem={item => (
      <List.Item
        key={item.articleId}
        actions={[
          <IconText icon={EyeOutlined} text={item.view} key="list-vertical-star-o" />,
          <IconText icon={MessageOutlined} text={item.comments} key="list-vertical-like-o" />,
          <IconText icon={LikeOutlined}  text={item.thumb} key="list-vertical-message" />,
        ]}
        extra={
          <img
            width={272}
            height={200}
            style={{objectFit:'cover'}}
            alt="logo"
            src={item.articlImg}
          />
        }
      >
        <List.Item.Meta
           avatar={<Avatar src={item.authorImg} />}
          title={<a href={item.href}>{item.articleTitle}</a>}
        />
        {
           <span className='found-list-content-n'>
             {item.articleContent}
           </span> }
      </List.Item>
    )}
  />
  </div>
            </div>
)
}
}