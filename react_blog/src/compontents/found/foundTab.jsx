import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
//import store from 
import { observer } from 'mobx-react'
import { Tabs } from 'antd';
import { SendOutlined, MessageOutlined, HeartOutlined ,CalendarOutlined} from '@ant-design/icons';
import FoundViewws from './foundViewed'
import FoundComments from './foundComments'
import FoundCollect from './foundCollect'
const { TabPane } = Tabs;
@observer
export default class FoundTab extends Component {
    render() {
        return (
            <>
              <p className='found-list-header'>
                <span><CalendarOutlined /></span>
                <span>受欢迎</span>
            </p>
            <Tabs defaultActiveKey="1">
           
            <TabPane
              tab={
                <span >
                 <span><SendOutlined /></span>
                  <span>最多点击</span> 
                </span>
              }
              key="1"
            >
             <FoundViewws />
            </TabPane>
            <TabPane
              tab={
                <span >
                 <span><MessageOutlined /></span> 
                 <span>最多评论</span> 
                </span>
              }
              key="2"
            >
              <FoundComments />
            </TabPane>
            <TabPane
              tab={
                <span >
                  <span><HeartOutlined /></span>
                  <span>最多收藏</span>
                </span>
              }
              key="3"
            >
              <FoundCollect />
            </TabPane>
          </Tabs>
          </>
        
)
}
}