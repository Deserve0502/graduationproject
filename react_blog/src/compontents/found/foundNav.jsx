import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
import store from '../../store/index'
import { CaretUpOutlined,DownOutlined,AudioOutlined } from '@ant-design/icons';
import { Menu, Dropdown } from 'antd';
import { Input, Space } from 'antd';

import '../../style/found.css'
import { observer } from 'mobx-react'
const { Search } = Input;
const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: '#1890ff',
      }}
    />
  );
  
  const onSearch = value => console.log(value);
const menu = (
    <Menu>
     <Menu.Item onClick={()=>store.getFoundNavSorce('综合')}>
        <span  >
          综合
              {/* <span className='found-nav-up'>
              <CaretUpOutlined/>
              </span> */}
          
        </span>
      </Menu.Item>
    <Menu.Item onClick={()=>store.getFoundNavSorce('最新')}>
        <span  >
          最新 
          <span className='found-nav-up'>
              <CaretUpOutlined/>
              </span>
          
        </span>
      </Menu.Item>
      <Menu.Item onClick={()=>store.getFoundNavSorce('热度')}>
      <span >
          热度 
          <span className='found-nav-up'>
              <CaretUpOutlined/>
              </span>
          
        </span>
      </Menu.Item>
      <Menu.Item onClick={()=>store.getFoundNavSorce('收藏量')}>
        <span >
          收藏量 
          <span className='found-nav-up'>
              <CaretUpOutlined/>
              </span>
          
        </span>
      </Menu.Item>
      <Menu.Item onClick={()=>store.getFoundNavSorce('回复量')}>
        <span >
          回复量 
          <span className='found-nav-up'>
              <CaretUpOutlined/>
              </span>
          
        </span>
      </Menu.Item>
      
     
    </Menu>
  );
@observer
export default class FoundNav extends Component {
    render() {
        return (
            <div className='found-nav-banner'>
<img src="../foundImg/all.jpg" alt="" />
<div className='found-nav-list-con safe'>
<div style={{display:'flex'}}>
<div className='found-cate-list'>
    <span>全部</span>
 
    <span>爱好</span>
   
    <span>时尚</span>
   
    <span>旅行</span>
   
    <span>冒险</span>
   
    <span>食物</span>
    
    <span>运动</span>
  
    <span>生活方式</span>
  
    <span>科技</span>
</div>
<div className='found-cate-sorce'>
    {/* <span>热度
    <span><CaretUpOutlined /></span>
    </span>
    <span>收藏量
    <span><CaretUpOutlined /></span>
    </span>
    <span>回复量
    <span><CaretUpOutlined /></span>
    </span>
    <span>时间范围
    <span><CaretUpOutlined /></span>
    </span> */}
    <Dropdown overlay={menu}>
    <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
   {store.foundNavSorce} 
    <DownOutlined style={{marginLeft:'5px'}}/>
    </a>
  </Dropdown>
</div>
</div>
<div className='found-search'>
<Search placeholder="搜索" onSearch={onSearch} style={{ width: 200 }} />
</div>
</div>
            </div>
)
}
}