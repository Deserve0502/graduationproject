import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
//import store from 
import store from '../../store'
import { BlockOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react'
import { Drawer, Button, Radio, Space } from 'antd';
import Model from '../../model/model'
import { List, Avatar, Skeleton,Spin, message } from 'antd';



// const fakeDataUrl = 'https://randomuser.me/api/?results=5&inc=name,gender,email,nat&noinfo';

@observer
export default class FoundList extends Component { 
  model = new Model();

      
      componentDidMount(){
        store.getFoundList();
      }
       
     
    
   
    
       
  
    
  

    onClose = () => {
     this.model.FoundVisible = false;
    };
  
  
    render() {
      const { showDrawer, FoundVisible, FoundDrawList , onLoadMore, warning} = this.model;
    const loadMore = (
        <div
          style={{
            textAlign: 'center',
            marginTop: 12,
            height: 32,
            lineHeight: '32px',
          }}
        >
          <Button onClick={onLoadMore}>查看更多</Button>
        </div>
    )
    
        return (
            <div className='found-list-contain safe'>
      
    
            <p className='found-list-header'>
                <span><BlockOutlined /></span>
                <span>类别</span>
            </p>
            <div style={{display:'flex',flexWrap:'wrap',justifyContent:'space-around'}}>

            { 
                    
                    store.foundList.map((ele, index) => {
                      return (
                        <div className='found-list' key={index} >
                       
            <div className='found-list-img' >
                <img src={ele.img} alt=""/>
            </div>
            <div className='found-list-cate'>
                <span>{ele.sumrise}</span>
                <span className='suggested-users-list-attention'>关注专题</span>
            </div>
            <div className='found-list-content'>
            { 
                    
                    ele.article.map((ele, index) => {
                      return (
                          <div key={index}>
                        <div className='found-list-article'
                       
                        >
                <span className='found-list-content-label'>
                    {ele.label}
                </span>
                <span>
                {ele.articleTitle}
                </span>
                   
                </div>

               
                </div>
               
                
                      )
                    })}
                   
             


            </div>
            <div style={{display:'flex',justifyContent:'center',alignItems:'center',padding:'0 0 20px 0'}}>
    <div className='trending-lookmore' style={{zIndex:'0',fontSize:'14px'}} onClick={()=>showDrawer(ele)}>查看更多</div>
    <Drawer
          title={FoundDrawList.sumrise}
          placement='right'
          closable={false}
          onClose={this.onClose}
          visible={FoundVisible}
          width={'500px'}
        >
        <>
       
          <List
            dataSource={this.model.FoundLoadList}
            // className="demo-loadmore-list"
        loading={false}
        itemLayout="horizontal"
        loadMore={loadMore}
       

            renderItem={item => (
              <List.Item key={item.id}>
                <List.Item.Meta
                  avatar={
                    <Avatar src={item.articlImg} />
                  }
                  title={<a href="https://ant.design">{item.articleTitle} <span className='found-list-content-label'>
                    {item.label}
                </span></a>}
                  description={item.authorName}
                />
                <div className='suggested-users-list-attention'>点击查看</div>
              </List.Item>
            )}
          >
           
          </List>
       

   
 

</>
          
        </Drawer>
</div>

            </div>
           
                      )
                    }
                    )}
           

          
       
            </div>
           
       
            </div>
)
}
}