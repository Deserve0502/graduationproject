import React, { Component } from 'react'
import QueueAnim from 'rc-queue-anim';
import { OverPack} from 'rc-scroll-anim';
import '../../style/index.css'
import { observer } from 'mobx-react';
import store from '../../store'
import My from '../../compontents/user/my'
import SocialExt from '../../compontents/user/socialExt'
@observer
export default class TrendingPost extends Component {
    state = {
        
    }

    componentDidMount(){
        store.getTrendingArticle();
     }

    render() {
        return (
            <div className='trending-container safe clear '>
              
               
                <OverPack style={{ height: 200 }}
                    always={false}
                >
                    <div className='trending-page '>
                    <div className='index-page-title'>
                    <p>最新发帖</p>
                    <p>Trending Post</p>
                </div>
                    <QueueAnim key="queue"
                        leaveReverse
                        style={{width:'120%',marginLeft:'-100px'  }}
                    >
                  <div style={{position:'absolute',right:'-73px'}}>
                    <My />
                    <SocialExt />

                  </div>
{
store.trendingArticle.map((ele, index) => {
                      return (
                        <div key={ele.id} className="trending-post" >
                        <span className='trending-up'>
                        </span>
                        <div className='trending-img'>
                            <img src={ele.img}/>
                            <span className='trending-reply'>
                                <span>{ele.replyNum}</span>
                                <span>评论</span>
                            </span>
                            <span className='tranding-classify'>
                               {ele.classify}
                            </span>
                        </div>
                        <div className='trending-content'>
                        <div className='trending-title'>
                            {ele.title}
                        </div>
                        <div className='trending-date'>
                            <span>
                                创建于 ⦁ 
                            </span>
                            <span className='create-date'> {ele.date}</span>
                        </div>
                        <div className='trending-articl'>
                       {ele.article}

                        </div>
                        <div className='trending-lookmore'>
                            查看更多
                        </div>

                        </div>
                       
                        </div>
                      )}
                     
                      )
                    
                     
}




                    </QueueAnim>
                    <div >
                    {/* //留位子 查看更多 */}
                        <span>......</span>
                    </div>

                    <div style={{clear:'both'}}></div>
                    </div>
                </OverPack>
            </div>
        )
    }
}