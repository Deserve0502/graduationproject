import { observable, computed, action } from "mobx";
import http from '../http/index'

class AppStore {

    @observable scrollshow = 'false';

    //发现页最多点击
    @observable foundViewed = [];

    //发现页最多评论
    @observable foundComments = [];

    //发现页最多收藏
    @observable foundCollect = [];

    //最新博客
    @observable trendingArticle = [];

    //好友动态
    @observable socialList = [];

    //个人标签
    @observable userLabel = [];

    //个人信息
    @observable authorMess = {};

    //热门用户
    @observable hotUsers = [];
    
    //积分榜
    @observable sorceBoard = [];

    //发现页专题
    @observable foundList = [];

    //发现页点击查看更多
    @observable crrentList = [];

    @observable foundNavSorce = '排序';
   

    @computed get desc() {
        return `${this.time}还有${this.todos.length}条任务待完成`
    }
    //意味着当time还有todos发生变化之后，就会重新自动执行，会返回一个新的结果

    @action changeScroll(boolean) {
        this.scrollshow = boolean
    }

    //发现页最多点击
    @action getFoundViewed() {
        http.get('api/foundViewed/'
        ).then(response => {
            this.foundViewed = response;
        })
    }

    //发现页最多评论
    @action getFoundComments() {
        http.get('api/foundComments/'
        ).then(response => {
            this.foundComments = response;
        })
    }

     //发现页最多收藏
     @action getFoundCollect() {
        http.get('api/foundCollect/'
        ).then(response => {
            this.foundCollect = response;
        })
    }

    //首页最新发帖
    @action getTrendingArticle() {
        http.get('api/trendingPost/'
        ).then(response => {
            this.trendingArticle = response;
        })
    }

    //获取个人标签
    @action getFoundTips(authorId) {
        http.get(`api/${authorId}/`
        ).then(response => { 
            this.authorMess = response;
        }
        )
    }

   //获取积分排行
    @action getFoundNavSorce(classify,i){
        this.foundNavSorce = classify;
    }

    //好友动态
    @action getSocialList(){
        http.get('api/socialList/'
        ).then(response => {
            this.socialList = response;
        })
    }

    //获取个人标签
    @action getUserLabel(){
        http.get('api/userLabel/'
        ).then(response => {
            this.userLabel = response;
        })
    }

    //获取热门用户
    @action getHotUsers(){
        http.get('api/hotUsers/'
        ).then(response => {
            this.hotUsers = response;
        })
    }

    //积分榜
    @action getSorceBoard(){
        http.get('api/sorceBoard/'
        ).then(response => {
            this.sorceBoard = response;
        })
    }

    @action getFoundList(){
        http.get('api/foundList/'
        ).then(response => {
            this.foundList = response;
            })
    }

    //点击展开
}

export default new AppStore();

