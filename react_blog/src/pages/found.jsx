
import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
//import store from 
import QueueAnim from 'rc-queue-anim';
import { Parallax } from 'rc-scroll-anim';
import ParticlesBg from 'particles-bg'
import store from '../store'
import Goback from '../common/goTop'
import Footer from '../common/footer'
import ScrollNav from '../common/scrollNav'
import IndexNav from '../common/indexNav'
import FoundTab from '../compontents/found/foundTab'
import FoundList from '../compontents/found/foundList'
import { observer } from 'mobx-react'
@observer
export default class Found extends Component {
    componentDidMount(){
        store.changeScroll('false');
       
    }
    state = {
        show: true,

    }
    render() {
        return (
            <>
            <Goback />
            <ScrollNav/>
                <div style={{  
                  backgroundColor:' #f7f7f7',
                position:'relative' }}>
              
                <Footer  />
                
        <QueueAnim className="demo-content">
            {this.state.show ? [
                <QueueAnim  
                key='foundcontain' >
                    <div key="found" >
                        <QueueAnim component="ul">
                            <IndexNav />
                            {/* <FoundNav /> */}
                            {/* <FoundCateGoryList/> */}
                            <div className='found-nav-banner'>
            <img src="../foundImg/all.jpg" alt="" />
            </div>
            <div className='safe' style={{paddingBottom:'400px'}}>
                <FoundList />
               <FoundTab />
            </div>

                        </QueueAnim>
                    </div>
                </QueueAnim>,
            
                
            ] : null}
        </QueueAnim>
                </div>
            </>
)
}
}




