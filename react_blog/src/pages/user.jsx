import React, { Component } from 'react'
//import ReactDom from 'react-dom'
//import PropTypes from 'prop-types'
//import axios from 'axios'
//import { NavLink, Switch, Route, Redirect, ,BrowserRouter } from 'react-router-dom'
//import store from 
import QueueAnim from 'rc-queue-anim';
import { Parallax } from 'rc-scroll-anim';
import ParticlesBg from 'particles-bg'
import store from '../store'
import Goback from '../common/goTop'
import Footer from '../common/footer'
import ScrollNav from '../common/scrollNav'
import IndexNav from '../common/indexNav'
import '../style/user.css'
// import { Pagination } from 'antd';
import My from '../compontents/user/my'
import SocialExt from '../compontents/user/socialExt'
import Userlabel from '../compontents/user/label'
import HotUser from '../compontents/user/hotUser'
import SorceBoard from '../compontents/user/sorceBoard'
import UserSearch from '../compontents/user/search'
import { observer } from 'mobx-react'
@observer
export default class Found extends Component {
    componentDidMount(){
        store.changeScroll('false');
       
    }
    state = {
        show: true,

    }
    render() {
        return (
            <>
            <Goback />
            <ScrollNav/>
                <div style={{ height: '2751px', 
                  backgroundColor:' #f7f7f7',
                position:'relative' }}>
                <Footer  />
                
    <QueueAnim className="demo-content">
        {this.state.show ? [
            <QueueAnim  
            key='foundcontain' >
                <div key="found" >
                    <QueueAnim component="ul">
                        <IndexNav />
             <div className='found-nav-banner'>
            <img src="../userImg/user.jpg" alt="" />
            </div>
            <div className='user-contain safe'>
            <div  className='user-contain-left'>
            <My />
            <Userlabel />
            <SocialExt />

            </div>

            <div className='user-contain-right'>
            <UserSearch />
          <HotUser />
          <SorceBoard />
            </div>
            </div>
           
                


                    </QueueAnim>
                </div>
            </QueueAnim>,
                       
                          
                        ] : null}
                    </QueueAnim>
                </div>
            </>
)
}
}




