import React, { Component } from 'react'
import QueueAnim from 'rc-queue-anim';
import { Parallax } from 'rc-scroll-anim';
import ParticlesBg from 'particles-bg'
import store from '../store'
import Goback from '../common/goTop'
import Footer from '../common/footer'
import ScrollNav from '../common/scrollNav'
import IndexNav from '../common/indexNav'
import FoundNav from '../compontents/found/foundNav'
import FoundCateGoryList from '../compontents/found/foundCategoryList'
// import { Pagination } from 'antd';
import '../style/circle.css'

import { observer } from 'mobx-react'
@observer
export default class Found extends Component {
    componentDidMount() {
        store.changeScroll('false');

    }
    state = {
        show: true,

    }
    render() {
        return (
            <>
    <Goback />
    <ScrollNav />
    <div style={{
        height: '2751px',
        backgroundColor: ' #f7f7f7',
        position: 'relative'
                }}>

        <Footer />

        {<QueueAnim className="demo-content">
            {this.state.show ? [
                <QueueAnim
                    key='foundcontain' >
                    <div key="found" >
                        <QueueAnim component="ul">
                            <IndexNav

                            />
                        <div className='circle-nav-banner found-nav-banner' style={{ zIndex: '0' }}>
                            <img src="../circleImg/circle.jpg" alt="" />
                        </div>
                    



                        </QueueAnim>
                    </div>
                </QueueAnim>,


            ] : null}
        </QueueAnim>
        }
    </div>
    
            </>
        )
    }
}