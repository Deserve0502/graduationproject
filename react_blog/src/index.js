import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './antd.min.css'
import '../node_modules/element-react/'
import Router from '../src/router/index'
import 'element-theme-default';
// import reportWebVitals from './reportWebVitals';

ReactDOM.render(
 <Router />
  ,
  document.getElementById('root')
);


